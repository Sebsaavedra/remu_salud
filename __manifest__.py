# -*- coding: utf-8 -*-
{
    'name': "Remu_salud",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Modulo remuneraciones sector salud - Chile
    """,

    'author': "Sebastian Saavedra Muñoz",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['hr_contract','hr_payroll',],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/bienios_view.xml',
        'views/mantenedor_dptos_view.xml',
    ],
    'application': True,
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}