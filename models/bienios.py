# -*- coding: utf-8 -*-
from datetime import date


from odoo import models, fields, api
from datetime import datetime
from time import strptime



class Bienios(models.Model):
    _inherit = 'hr.contract'

    categoria = fields.Selection([('cat_a', 'CAT. A'),('cat_b', 'CAT. B'),('cat_c', 'CAT. C'),('cat_d', 'CAT. D'),('cat_e', 'CAT. E'),('cat_f', 'CAT. F')])
    puntos_capacitacion = fields.Integer(string="Puntos Capacitación")
    puntos_asimilados = fields.Integer(string="Puntos Asimilados")
    bienios = fields.Integer(compute='_calculo_bienio', readonly=True, string="Bienios" )
    fecha_ingreso_cargo = fields.Date(string="Fecha Ingreso al Cargo")
    puntos_totales = fields.Integer(compute='_calculo_nivel', readonly=True, string="Puntos Totales")
    # date_to = fields.Many2one('hr.payroll.date_to', string="Fecha Periodo Nomina")
    sueldo_base = fields.Float(compute='_calculo_sueldo_bruto', string="Sueldo Base", readonly=True)
    nivel = fields.Integer(compute='_calculo_sueldo_bruto', string="Nivel", readonly=True)

    @api.depends('date_start','puntos_totales',)
    def _calculo_bienio(self):

        fecha_today = datetime.strptime(str(fields.Date.today() ), '%Y-%m-%d')
        #fecha_ingreso_cargo =  datetime.strptime(str(self.fecha_ingreso_cargo), '%Y-%m-%d')
        fecha_ingreso_cargo =  datetime.strptime(str(date(2005, 11, 2)), '%Y-%m-%d')

        diferencia_fechas_dias = abs(fecha_today - fecha_ingreso_cargo).days
        diferencia_fechas_meses = (fecha_today.year - fecha_ingreso_cargo.year) * 12 + fecha_today.month - fecha_ingreso_cargo.month
        # years = int(diferencia_fechas_dias / 365.2425)

        for record in self:
            record.bienios = int((diferencia_fechas_meses-1)/24)


    @api.depends('bienios', 'puntos_asimilados', 'puntos_capacitacion' )
    def _calculo_nivel(self):
        ptos_bienios = 0

        tabla_bienios = {'1': '533.00',
                         '2': '1067.00',
                         '3': '1600.00',
                         '4': '2133.00',
                         '5': '2667.00',
                         '6': '3200.00',
                         '7': '3733.00',
                         '8': '4267.00',
                         '9': '4800.00',
                         '10': '5333.00',
                         '11': '5867.00',
                         '12': '6400.00',
                         '13': '6933.00',
                         '14': '7467.00',
                         '15': '8000.00'}
        n_bienios = tabla_bienios.keys()

        for i in n_bienios:
            if str(self.bienios) == i:
                 ptos_bienios = (int(float(tabla_bienios[i])))
                 self.puntos_totales = ptos_bienios + self.puntos_asimilados + self.puntos_capacitacion


    @api.depends('puntos_totales', 'sueldo_base', 'categoria')
    def _calculo_sueldo_bruto(self):
        if self.categoria == 'cat_c':
            result = self.sueldo_c_d_e_f(self.puntos_totales, self.categoria)
            self.sueldo_base = float(result['sueldo'])
            self.nivel = int(result['nivel'])

    def prueba(self):

        return 50

    def sueldo_c_d_e_f(self, puntos_totales, categoria):
        result = dict()

        ptos_totales = puntos_totales
        cat = categoria
        categoria = cat.split('_')

        tabla_sueldo = {'15': ['0.00', '803.00'],
                        '14': ['803.01', '1774.00'],
                        '13': ['1774.01', '2745.00'],
                        '12': ['2745.01', '3716.00'],
                        '11': ['3716.01', '4687.00'],
                        '10': ['4687.01', '5648.00'],
                        '9': ['5648.01', '6579.00'],
                        '8': ['6579.01', '7460.00'],
                        '7': ['7460.01', '8311.00'],
                        '6': ['8311.01', '9062.00'],
                        '5': ['9062.01', '9743.00'],
                        '4': ['9743.01', '10364.00'],
                        '3': ['10364.01', '10926.00'],
                        '2': ['10926.01', '11488.00'],
                        '1': ['11488.01', '12050.00']
                        }
        sueldos_cat_c = {'15': ['226459'],
                         '14': ['237522'],
                         '13': ['255077'],
                         '12': ['272638'],
                         '11': ['290196'],
                         '10': ['307750'],
                         '9': ['335721'],
                         '8': ['365164'],
                         '7': ['360425'],
                         '6': ['377984'],
                         '5': ['395542'],
                         '4': ['405436'],
                         '3': ['415335'],
                         '2': ['426406'],
                         '1': ['441263']
                         }
        sueldos_cat_d = {'15': ['211301'],
                         '14': ['228168'],
                         '13': ['245032'],
                         '12': ['261902'],
                         '11': ['278766'],
                         '10': ['295633'],
                         '9': ['312500'],
                         '8': ['329365'],
                         '7': ['346231'],
                         '6': ['363099'],
                         '5': ['379963'],
                         '4': ['389468'],
                         '3': ['398978'],
                         '2': ['409612'],
                         '1': ['423883']
                         }
        sueldos_cat_e = {'15': ['212844'],
                         '14': ['216833'],
                         '13': ['232863'],
                         '12': ['248889'],
                         '11': ['264918'],
                         '10': ['280946'],
                         '9': ['296974'],
                         '8': ['313004'],
                         '7': ['329030'],
                         '6': ['345061'],
                         '5': ['361089'],
                         '4': ['370808'],
                         '3': ['381696'],
                         '2': ['393050'],
                         '1': ['406509']
                         }
        sueldos_cat_f = {'15': ['191287'],
                         '14': ['191287'],
                         '13': ['194279'],
                         '12': ['207652'],
                         '11': ['221025'],
                         '10': ['234397'],
                         '9': ['247769'],
                         '8': ['261143'],
                         '7': ['274514'],
                         '6': ['287888'],
                         '5': ['301263'],
                         '4': ['314635'],
                         '3': ['318455'],
                         '2': ['331436'],
                         '1': ['344418']
                         }

        for nivel, puntaje in tabla_sueldo.items():
            minimo = puntaje[0]
            maximo = puntaje[1]

            if ptos_totales >= float(minimo) and ptos_totales <= float(maximo):
                if categoria[1] == 'c':
                    result['sueldo'] = float(sueldos_cat_c[nivel][0])
                    result['nivel'] = nivel
                    return result
                if categoria[1] == 'd':
                    result['sueldo'] = float(sueldos_cat_c[nivel][0])
                    result['nivel'] = nivel
                    return result
                if categoria[1] == 'e':
                    result['sueldo'] = float(sueldos_cat_c[nivel][0])
                    result['nivel'] = nivel
                    return result
                if categoria[1] == 'f':
                    result['sueldo'] = float(sueldos_cat_c[nivel][0])
                    result['nivel'] = nivel
                    return result


