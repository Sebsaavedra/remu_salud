# -*- coding: utf-8 -*-

from odoo import models, fields, api


class MantenedorDptos(models.Model):
    _inherit = 'hr.department'

    desDif = fields.Float(string="Desempeño Dificil")
